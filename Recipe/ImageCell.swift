//
//  ImageCell.swift
//  Recipe
//
//  Created by Loka on 26/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

protocol AddPhotoDelegate{
    func cellBeginAddPhoto(cell: ImageCell )
}

class ImageCell: UITableViewCell {

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var photoView: UIImageView!
    
    var delegate: AddPhotoDelegate!
    
    @IBAction func addButtonPressed(sender: AnyObject) {
       self.delegate.cellBeginAddPhoto(self)
    }
}

extension ImageCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        photoView.image = image
    }
}