//
//  EditViewController.swift
//  Recipe
//
//  Created by Loka on 26/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

protocol AddRecipeDelegate{
    func viewController(vc: EditViewController, didAddRecipe recipe: Recipe)
}

class EditViewController: UIViewController {
    
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subContrainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stepTextView: UITextView!
    @IBOutlet weak var ingredientTextField: UITextField!
    @IBOutlet weak var deleteIngredientButton: UIButton!
    @IBOutlet weak var addIngredientButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var subContainerView: UIView!
    
    var tapGestureRecognizer: UITapGestureRecognizer!
    var longPress  :UILongPressGestureRecognizer!
    var recipe: Recipe!
    var delegate: AddRecipeDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavigationBar()
        nameTextField.delegate = self
        nameTextField.tag = 0
        ingredientTextField.delegate = self
        ingredientTextField.tag = 1
        stepTextView.delegate = self
        pickerView.dataSource = self
        pickerView.delegate = self
        if recipe == nil {
            recipe = Recipe.init()
            recipe.ingredients =  [String]()
            recipe.photoImage = UIImage.init(named: "Placeholder")
        }else{
            fillInForEditMode()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        addLongPressRecognizer()
    }
    
    override func viewDidDisappear(animated: Bool) {
        imageView.removeGestureRecognizer(longPress)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addIngredient(sender: AnyObject) {
        let occupied = checkTextFieldisOccupied()
        guard occupied == true else {
            let alert = UIAlertController.init(title: "Cannot add new ingredients", message: "Previous ingredient is still empty", preferredStyle: .Alert)
            let action = UIAlertAction.init(title: "OK", style: .Default, handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        var frame = ingredientTextField.frame
        let totalOffset = (frame.height + 8)*CGFloat(getNumberOfTextFieldsforIngredients())
        frame.origin.y = frame.origin.y + totalOffset
        let textField = UITextField.init(frame: frame)
        textField.layer.borderColor = ingredientTextField.layer.borderColor
        textField.layer.borderWidth = ingredientTextField.layer.borderWidth
        textField.borderStyle = ingredientTextField.borderStyle
        textField.tag = getNumberOfTextFieldsforIngredients() + 1
        textField.delegate = self
        containerView.addSubview(textField)
        containerViewHeight.constant = containerViewHeight.constant + frame.height + 8
        subContrainerTopConstraint.constant = subContrainerTopConstraint.constant + frame.height + 8
        containerView.sizeToFit()
    }
    
    func checkTextFieldisOccupied()-> Bool{
        let textFields = containerView.subviews.filter{$0 is UITextField}
        let textField:UITextField = textFields.filter{$0.tag == getNumberOfTextFieldsforIngredients()}.first as! UITextField
        if textField.text == "" {
            return false
        }else {
            return true
        }
    }
    
    @IBAction func deleteIngredient(sender: UIButton) {
        guard getNumberOfTextFieldsforIngredients() > 1 else {
            return
        }
        if recipe.ingredients!.count == getNumberOfTextFieldsforIngredients(){
            recipe.ingredients?.removeLast()
        }
        let textFields = containerView.subviews.filter{$0 is UITextField }
        let textField = textFields.filter{$0.tag == getNumberOfTextFieldsforIngredients()}.first!
        textField.removeFromSuperview()
        subContrainerTopConstraint.constant = subContrainerTopConstraint.constant - textField.frame.height - 8
    }
    
    func addNavigationBar(){
        let frame = CGRectMake(0, 0, self.view.frame.width, 64)
        let navigationbar = UINavigationBar.init(frame: frame)
        let item = UINavigationItem.init()
        item.title = "Create new recipe"
        let doneButton = UIBarButtonItem.init(title: "Done", style: .Plain, target: self, action: #selector(done))
        item.leftBarButtonItem = doneButton
        let cancelButton = UIBarButtonItem.init(title: "Cancel", style: .Plain, target: self, action: #selector(cancel))
        item.rightBarButtonItem = cancelButton
        navigationbar.items = [item]
        self.view.addSubview(navigationbar)
    }
    
    func done(){
        self.view.endEditing(true)
        guard recipe.name != nil && recipe.name != "" else {
            let alert = UIAlertController.init(title: "Cannot add recipe", message: "Name is empty", preferredStyle: .Alert)
            let action = UIAlertAction.init(title: "OK", style: .Default , handler: nil)
            alert.addAction(action)
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        self.delegate.viewController(self, didAddRecipe: recipe)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancel(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getNumberOfTextFieldsforIngredients()->Int{
        let textFields = containerView.subviews.filter{$0 is UITextField}
        return textFields.count - 1
    }
    
    func fillInForEditMode(){
        let navigationBar = view.subviews.filter{
            $0 is UINavigationBar
        }.first as! UINavigationBar
        let item = navigationBar.items?.first!
        item?.title = "Edit Recipe"
        let recipe = self.recipe!
        nameTextField.text = recipe.name
        nameTextField.enabled = false
        stepTextView.text = recipe.steps
        imageView.image = recipe.photoImage
        if recipe.type != nil {
            pickerView.selectRow(recipe.type, inComponent: 0, animated: true)
        }
        guard recipe.ingredients != nil else {
            return
        }
        let numberOfingredients = recipe.ingredients!.count
        for i in 0...numberOfingredients - 1{
            let textFields = containerView.subviews.filter{$0 is UITextField}
            let textField = textFields.filter{$0.tag == i+1}.first as! UITextField
            textField.text = recipe.ingredients![i]
            if i != numberOfingredients - 1{
                 self.addIngredient(self)
            }
        }
        
        
    }
}

extension EditViewController: UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print ("editing")
        if textField.tag == 0 {
            recipe.name = textField.text
        }
        if textField.tag > 0 {
            print (recipe.ingredients!.count)
            print (textField.tag)
            if recipe.ingredients!.count >= textField.tag{
                recipe.ingredients![textField.tag - 1] = textField.text!
            }else{
                recipe.ingredients?.append(textField.text!)
            }
            
        }
        return true
    }
    

}

extension EditViewController: UITextViewDelegate{
    
    
    func textViewDidBeginEditing(textView: UITextView) {
        tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapOutsideTextView) )
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        recipe.steps = textView.text
        self.view.removeGestureRecognizer(tapGestureRecognizer)
    }
    
    func tapOutsideTextView(){
        stepTextView.resignFirstResponder()
    }
}

extension EditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func addLongPressRecognizer(){
        longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(selectImage))
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(longPress)
    }
    
    func selectImage(){
        let imagePicker = UIImagePickerController.init()
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        recipe.photoImage = image
        imageView.image = image
    }
}

extension EditViewController: UIPickerViewDataSource{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Recipe.RecipeType.count
    }
}

extension EditViewController: UIPickerViewDelegate{
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(Recipe.RecipeType.getEnumuration(row))
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        recipe.type = row
    }
}



