//
//  AddHeaderCell.swift
//  Recipe
//
//  Created by Loka on 25/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

protocol ModifyElementDelegate {
    func addElement(cell: AddHeaderCell)
    func removeElement(cell: AddHeaderCell)
}



class AddHeaderCell: UITableViewCell {
    
    var delegate:  ModifyElementDelegate!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func addButtonPressed(sender: UIButton) {
        self.delegate.addElement(self)
    }
    
    
    @IBAction func deleteButtonPressed(sender: UIButton) {
        self.delegate.removeElement(self)
    }
    
}
