//
//  ListCell.swift
//  Recipe
//
//  Created by Loka on 25/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var recipeImageView: UIImageView!
    var recipe: Recipe!{
        didSet{
            updateDisplay()
        }
    }
    
    func updateDisplay(){
        nameLabel.text = recipe.name
        recipeImageView.image = recipe.photoImage
    }
}
