//
//  recipe.swift
//  Recipe
//
//  Created by Loka on 25/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Recipe: NSObject, NSCoding{
    var name: String!
    var ingredients: [String]?
    var steps: String?
    var photoImage: UIImage?
    var type: Int!
    
    
    override init() {
        
    }
    
    init(name: String, type: Int){
        self.name = name
        self.type = type
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObjectForKey("name") as! String
        ingredients = aDecoder.decodeObjectForKey("ingredients") as? [String]
        steps = aDecoder.decodeObjectForKey("steps") as? String
        type = aDecoder.decodeIntegerForKey("type")
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(ingredients, forKey: "ingredients")
        aCoder.encodeObject(steps, forKey: "steps")
        aCoder.encodeInteger(type, forKey: "type")
    }
    
    enum RecipeType{
        case Vegetarian
        case FastFood
        case Healthy
        case NoCook
        case MakeAhead
        case All
        
        static var count = Int(RecipeType.All.hashValue + 1)
        
        static func getEnumuration(int: Int) -> RecipeType{
            switch int {
            case 0:
                return RecipeType.Vegetarian
            case 1:
                return RecipeType.FastFood
            case 2:
                return RecipeType.Healthy
            case 3:
                return RecipeType.NoCook
            case 4:
                return RecipeType.MakeAhead
            case 5:
                return RecipeType.All
            default:
               return RecipeType.All
            }
        }
    }
    
    class func getPath() -> String {
        let manager = NSFileManager.defaultManager()
        let dirPath = manager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
        return dirPath.URLByAppendingPathComponent("recipes").path!
    }

}

