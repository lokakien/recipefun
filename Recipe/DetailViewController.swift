//
//  DetailViewController.swift
//  Recipe
//
//  Created by Loka on 25/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

protocol DidModifyRecipeDelegate{
    func viewController(vc: DetailViewController, didModifyRecipe modifiedRecipe:Recipe)
}

class DetailViewController: UIViewController {
    var recipe: Recipe!
    var delegate: DidModifyRecipeDelegate!
    
    @IBOutlet weak var stepsTextView: UITextView!
    @IBOutlet weak var ingredientTextView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.topItem?.title = ""
        updateDisplay()

        // Do any additional setup after loading the view.
    }
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateDisplay() {
        self.title = recipe.name
        imageView.image = recipe.photoImage
        var ingredientString = ""
        if recipe.ingredients != nil{
            for ingredient in (recipe.ingredients)!{
                if ingredientString == recipe.ingredients?.first{
                    ingredientString = ingredient
                }
                ingredientString = "\(ingredientString)\r\n\(ingredient)"
            }
        }
        ingredientTextView.text = ingredientString
        ingredientTextView.sizeToFit()
        stepsTextView.text = recipe?.steps
        stepsTextView.sizeToFit()
    }
    
    @IBAction func editButtonPressed(sender: UIButton) {
        
        self.performSegueWithIdentifier("segueToEditVCfromDetail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToEditVCfromDetail" {
            let editVC = segue.destinationViewController as! EditViewController
            editVC.recipe = recipe
            editVC.delegate = self
        }
    }
    
}

extension DetailViewController: AddRecipeDelegate {
    func viewController(vc: EditViewController, didAddRecipe recipe: Recipe) {
        self.recipe = recipe
        updateDisplay()
    }
}


