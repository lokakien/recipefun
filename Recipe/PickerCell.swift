//
//  PickerCell.swift
//  Recipe
//
//  Created by Loka on 26/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class PickerCell: UITableViewCell {

    @IBOutlet weak var pickerView: UIPickerView!
  
}
