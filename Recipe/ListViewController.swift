//
//  ViewController.swift
//  Recipe
//
//  Created by Loka on 25/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var listTableView: UITableView!
    var recipes: [Recipe] = []
    var filterRecipes:[Recipe] = []
    var showPicker: Bool = false
    var pickerView: UIPickerView?
    var doneButton: UIBarButtonItem?
    var typeEnum: Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Recipe List"
        preloadData()
        typeEnum = Recipe.RecipeType.All.hashValue
        reloadData(typeEnum!)
        listTableView.dataSource = self
        listTableView.delegate = self
        doneDeleteRecipe()
    }
    
    override func viewWillAppear(animated: Bool) {
        reloadData(typeEnum!)
    }
    
    func preloadData(){
        let ingredientArray = ["meat", "vege", "fish"]
        let recipe1 = Recipe.init(name: "Coleslaw", type: Recipe.RecipeType.Healthy.hashValue)
        recipe1.photoImage = UIImage.init(named: "Coleslaw")
        recipe1.ingredients = ingredientArray
        recipe1.steps = "Crush the ramen noodles in a bowl, and stir in the beef seasoning packet, sunflower seeds, almonds, green onions, oil and vinegar dressing, and sugar, mixing until the sugar is dissolved. Place the coleslaw mix into a large salad bowl, pour the dressing mixture over, and toss lightly to combine well. Refrigerate at least 1 hour before serving."
        let recipe2 = Recipe.init(name: "Pizza", type: Recipe.RecipeType.FastFood.hashValue)
        recipe2.photoImage = UIImage.init(named: "Pizza")
        recipe2.ingredients = ingredientArray
        recipe2.steps = "Pile the flour and salt on to a clean surface and make an 18cm well in the centre. Add your yeast and sugar to the lukewarm water, mix up with a fork and leave for a few minutes, then pour into the well."
        recipes.append(recipe1)
        recipes.append(recipe2)
    }
    
    
    @IBAction func editRecipe(sender: AnyObject) {
        self.performSegueWithIdentifier("segueToEditVC", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addRecipe(sender: AnyObject) {
        self.performSegueWithIdentifier("segueToAddVC", sender: self)
    }

    func deleteRecipe() {
        listTableView.setEditing(true, animated: true)
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: .Done, target: self, action: #selector(ListViewController.doneDeleteRecipe))
        self.navigationItem.leftBarButtonItem = doneButton
    }
    
    func doneDeleteRecipe(){
        listTableView.setEditing(false, animated: true)
        let editButton = UIBarButtonItem.init(barButtonSystemItem: .Edit, target: self, action: #selector(ListViewController.deleteRecipe))
        self.navigationItem.leftBarButtonItem = editButton
    }
    
    @IBAction func addPicker(sender: AnyObject) {
        showPicker = !showPicker
        if showPicker{
            let frame = CGRectMake(listTableView.frame.origin.x, listTableView.frame.origin.y, listTableView.frame.width, 150)
            pickerView = UIPickerView.init(frame: frame)
            pickerView!.backgroundColor = UIColor.whiteColor()
            pickerView!.dataSource = self
            pickerView!.delegate = self
            self.view.addSubview(pickerView!)
        }else {
            
            pickerView!.removeFromSuperview()
        }
        
    }
    
    
    func reloadData(typeEnum: Int){
        if typeEnum == Recipe.RecipeType.All.hashValue{
            filterRecipes = recipes
        }else{
            filterRecipes = recipes.filter({$0.type == typeEnum})
        }
        listTableView.reloadData()
    }
    

}

extension ListViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterRecipes.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("listCell", forIndexPath: indexPath) as! ListCell
        cell.recipe = filterRecipes[indexPath.item]
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            let name = filterRecipes[indexPath.row].name
            for i in 0...recipes.count{
                if recipes[i].name == name {
                    recipes.removeAtIndex(i)
                    reloadData(typeEnum!)
                }
            }
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
}

extension ListViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueToDetailVC", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToDetailVC"{
            let detailVC = segue.destinationViewController as! DetailViewController
            let indexPath = listTableView.indexPathForSelectedRow!
            detailVC.recipe = filterRecipes[indexPath.item]
            detailVC.delegate = self
        }
        
        if segue.identifier == "segueToEditVC"{
            let editVC = segue.destinationViewController as! EditViewController
            editVC.delegate = self
        }
    }
}

extension ListViewController: DidModifyRecipeDelegate{
    func viewController(vc: DetailViewController, didModifyRecipe modifiedRecipe: Recipe) {
        for i in 0...(recipes.count - 1) {
            if recipes[i].name == modifiedRecipe.name{
                recipes[i] = modifiedRecipe
                reloadData(typeEnum!)
            }
        
        }
    }
}

extension ListViewController: UIPickerViewDataSource{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return Recipe.RecipeType.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(Recipe.RecipeType.getEnumuration(row))
    }
}

extension ListViewController: UIPickerViewDelegate{
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        typeEnum = row
        reloadData(typeEnum!)
    }
}

extension ListViewController: AddRecipeDelegate{
    func viewController(vc: EditViewController, didAddRecipe recipe: Recipe) {
        recipes.append(recipe)
        reloadData(typeEnum!)
    }
}




